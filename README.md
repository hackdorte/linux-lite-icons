# Linux Lite Icons

My FREE artwork for the Linux Lite Operating System. 

Icons for Linux Lite applications and software.

You can customize the **Linux Lite Software** icons and make them **beautiful**.

### The Linux Lite

Linux Lite is a GNU/Linux operating system and you can get a free copy and install it on your devices and get started easily.

[Official Website](https://www.linuxliteos.com/)

[Software Repo](https://gitlab.com/linuxlite)

Start with Linux Lite today and enjoy.