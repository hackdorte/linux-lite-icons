#!/bin/bash
#
# Convert SVG source files to PNG icon files with Inkscape.
# =============================================================
# AUTHOR: Leonardo Laporte <//hackdorte//@yandex.com>
# SYSTEM: Linux Lite 5.
# DEPEND: Inkscape.
# =============================================================
#

clear

if [[ $UID == "0" ]]
then
	echo "=============================================="
	echo
	echo "++ OPS!"
	echo
	echo " Please do not run this as root."
	echo " Nothing Done."
	echo
	echo "++ Closed!"
	echo
	echo "=============================================="
	echo
	echo && exit
fi

INITIAL="ICONS"
SUBDIR="PNG"
DEFAULT="128"
XLARGE="64"
NORMAL="32"
MEDIUM="24"
XSMALL="16"

echo
echo "Starting conversion ( $DEFAULT x $DEFAULT )..."
echo

inkscape -z -w $DEFAULT -h $DEFAULT liteautologin.svg -e liteautologin.png
inkscape -z -w $DEFAULT -h $DEFAULT litedesktop.svg -e litedesktop.png
inkscape -z -w $DEFAULT -h $DEFAULT liteicon.svg -e liteicon.png
inkscape -z -w $DEFAULT -h $DEFAULT liteinfo.svg -e liteinfo.png
inkscape -z -w $DEFAULT -h $DEFAULT litemanual.svg -e litemanual.png
inkscape -z -w $DEFAULT -h $DEFAULT litenetworkshares.svg -e litenetworkshares.png
inkscape -z -w $DEFAULT -h $DEFAULT liteOEM.svg -e oem-lite.png
inkscape -z -w $DEFAULT -h $DEFAULT litesoftware.svg -e litesoftware.png
inkscape -z -w $DEFAULT -h $DEFAULT litesoundsglob.svg -e litesoundsglob.png
inkscape -z -w $DEFAULT -h $DEFAULT litesoundsloc.svg -e litesoundsloc.png
inkscape -z -w $DEFAULT -h $DEFAULT litesources.svg -e litesources.png
inkscape -z -w $DEFAULT -h $DEFAULT litesupport.svg -e litesupport.png
inkscape -z -w $DEFAULT -h $DEFAULT litesystemreport.svg -e litesystemreport.png
inkscape -z -w $DEFAULT -h $DEFAULT litetweaks.svg -e litetweaks.png
inkscape -z -w $DEFAULT -h $DEFAULT liteupdatesnotify.svg -e liteupdatesnotify.png
inkscape -z -w $DEFAULT -h $DEFAULT liteupdates.svg -e liteupdates.png
inkscape -z -w $DEFAULT -h $DEFAULT liteupgrade.svg -e liteupgrade.png
inkscape -z -w $DEFAULT -h $DEFAULT liteusermanager.svg -e liteusermanager.png
inkscape -z -w $DEFAULT -h $DEFAULT litewelcome.svg -e litewelcome.png
inkscape -z -w $DEFAULT -h $DEFAULT litewidget.svg -e litewidget.png

echo "All done."
echo
echo "Creating PNG directory ($DEFAULT) ..."

mkdir -p $DEFAULT
mv *.png $DEFAULT/

echo
echo "Wait..."
sleep 2

echo
echo "Starting conversion ( $XLARGE x $XLARGE )..."
echo

inkscape -z -w $XLARGE -h $XLARGE liteautologin.svg -e liteautologin.png
inkscape -z -w $XLARGE -h $XLARGE litedesktop.svg -e litedesktop.png
inkscape -z -w $XLARGE -h $XLARGE liteicon.svg -e liteicon.png
inkscape -z -w $XLARGE -h $XLARGE liteinfo.svg -e liteinfo.png
inkscape -z -w $XLARGE -h $XLARGE litemanual.svg -e litemanual.png
inkscape -z -w $XLARGE -h $XLARGE litenetworkshares.svg -e litenetworkshares.png
inkscape -z -w $XLARGE -h $XLARGE liteOEM.svg -e oem-lite.png
inkscape -z -w $XLARGE -h $XLARGE litesoftware.svg -e litesoftware.png
inkscape -z -w $XLARGE -h $XLARGE litesoundsglob.svg -e litesoundsglob.png
inkscape -z -w $XLARGE -h $XLARGE litesoundsloc.svg -e litesoundsloc.png
inkscape -z -w $XLARGE -h $XLARGE litesources.svg -e litesources.png
inkscape -z -w $XLARGE -h $XLARGE litesupport.svg -e litesupport.png
inkscape -z -w $XLARGE -h $XLARGE litesystemreport.svg -e litesystemreport.png
inkscape -z -w $XLARGE -h $XLARGE litetweaks.svg -e litetweaks.png
inkscape -z -w $XLARGE -h $XLARGE liteupdatesnotify.svg -e liteupdatesnotify.png
inkscape -z -w $XLARGE -h $XLARGE liteupdates.svg -e liteupdates.png
inkscape -z -w $XLARGE -h $XLARGE liteupgrade.svg -e liteupgrade.png
inkscape -z -w $XLARGE -h $XLARGE liteusermanager.svg -e liteusermanager.png
inkscape -z -w $XLARGE -h $XLARGE litewelcome.svg -e litewelcome.png
inkscape -z -w $XLARGE -h $XLARGE litewidget.svg -e litewidget.png

echo "All done."
echo
echo "Creating PNG directory ($XLARGE) ..."

mkdir -p $XLARGE
mv *.png $XLARGE/

echo
echo "Wait..."
sleep 2

echo
echo "Starting conversion ( $NORMAL x $NORMAL )..."
echo

inkscape -z -w $NORMAL -h $NORMAL liteautologin.svg -e liteautologin.png
inkscape -z -w $NORMAL -h $NORMAL litedesktop.svg -e litedesktop.png
inkscape -z -w $NORMAL -h $NORMAL liteicon.svg -e liteicon.png
inkscape -z -w $NORMAL -h $NORMAL liteinfo.svg -e liteinfo.png
inkscape -z -w $NORMAL -h $NORMAL litemanual.svg -e litemanual.png
inkscape -z -w $NORMAL -h $NORMAL litenetworkshares.svg -e litenetworkshares.png
inkscape -z -w $NORMAL -h $NORMAL liteOEM.svg -e oem-lite.png
inkscape -z -w $NORMAL -h $NORMAL litesoftware.svg -e litesoftware.png
inkscape -z -w $NORMAL -h $NORMAL litesoundsglob.svg -e litesoundsglob.png
inkscape -z -w $NORMAL -h $NORMAL litesoundsloc.svg -e litesoundsloc.png
inkscape -z -w $NORMAL -h $NORMAL litesources.svg -e litesources.png
inkscape -z -w $NORMAL -h $NORMAL litesupport.svg -e litesupport.png
inkscape -z -w $NORMAL -h $NORMAL litesystemreport.svg -e litesystemreport.png
inkscape -z -w $NORMAL -h $NORMAL litetweaks.svg -e litetweaks.png
inkscape -z -w $NORMAL -h $NORMAL liteupdatesnotify.svg -e liteupdatesnotify.png
inkscape -z -w $NORMAL -h $NORMAL liteupdates.svg -e liteupdates.png
inkscape -z -w $NORMAL -h $NORMAL liteupgrade.svg -e liteupgrade.png
inkscape -z -w $NORMAL -h $NORMAL liteusermanager.svg -e liteusermanager.png
inkscape -z -w $NORMAL -h $NORMAL litewelcome.svg -e litewelcome.png
inkscape -z -w $NORMAL -h $NORMAL litewidget.svg -e litewidget.png

echo "All done."
echo
echo "Creating PNG directory ($NORMAL) ..."

mkdir -p $NORMAL
mv *.png $NORMAL/

echo
echo "Wait..."
sleep 2

echo
echo "Starting conversion ( $MEDIUM x $MEDIUM )..."
echo

inkscape -z -w $MEDIUM -h $MEDIUM liteautologin.svg -e liteautologin.png
inkscape -z -w $MEDIUM -h $MEDIUM litedesktop.svg -e litedesktop.png
inkscape -z -w $MEDIUM -h $MEDIUM liteicon.svg -e liteicon.png
inkscape -z -w $MEDIUM -h $MEDIUM liteinfo.svg -e liteinfo.png
inkscape -z -w $MEDIUM -h $MEDIUM litemanual.svg -e litemanual.png
inkscape -z -w $MEDIUM -h $MEDIUM litenetworkshares.svg -e litenetworkshares.png
inkscape -z -w $MEDIUM -h $MEDIUM liteOEM.svg -e oem-lite.png
inkscape -z -w $MEDIUM -h $MEDIUM litesoftware.svg -e litesoftware.png
inkscape -z -w $MEDIUM -h $MEDIUM litesoundsglob.svg -e litesoundsglob.png
inkscape -z -w $MEDIUM -h $MEDIUM litesoundsloc.svg -e litesoundsloc.png
inkscape -z -w $MEDIUM -h $MEDIUM litesources.svg -e litesources.png
inkscape -z -w $MEDIUM -h $MEDIUM litesupport.svg -e litesupport.png
inkscape -z -w $MEDIUM -h $MEDIUM litesystemreport.svg -e litesystemreport.png
inkscape -z -w $MEDIUM -h $MEDIUM litetweaks.svg -e litetweaks.png
inkscape -z -w $MEDIUM -h $MEDIUM liteupdatesnotify.svg -e liteupdatesnotify.png
inkscape -z -w $MEDIUM -h $MEDIUM liteupdates.svg -e liteupdates.png
inkscape -z -w $MEDIUM -h $MEDIUM liteupgrade.svg -e liteupgrade.png
inkscape -z -w $MEDIUM -h $MEDIUM liteusermanager.svg -e liteusermanager.png
inkscape -z -w $MEDIUM -h $MEDIUM litewelcome.svg -e litewelcome.png
inkscape -z -w $MEDIUM -h $MEDIUM litewidget.svg -e litewidget.png

echo "All done."
echo
echo "Creating PNG directory ($MEDIUM) ..."

mkdir -p $MEDIUM
mv *.png $MEDIUM/

echo
echo "Wait..."
sleep 2

echo
echo "Starting conversion ( $XSMALL x $XSMALL )..."
echo

inkscape -z -w $XSMALL -h $XSMALL liteautologin.svg -e liteautologin.png
inkscape -z -w $XSMALL -h $XSMALL litedesktop.svg -e litedesktop.png
inkscape -z -w $XSMALL -h $XSMALL liteicon.svg -e liteicon.png
inkscape -z -w $XSMALL -h $XSMALL liteinfo.svg -e liteinfo.png
inkscape -z -w $XSMALL -h $XSMALL litemanual.svg -e litemanual.png
inkscape -z -w $XSMALL -h $XSMALL litenetworkshares.svg -e litenetworkshares.png
inkscape -z -w $XSMALL -h $XSMALL liteOEM.svg -e oem-lite.png
inkscape -z -w $XSMALL -h $XSMALL litesoftware.svg -e litesoftware.png
inkscape -z -w $XSMALL -h $XSMALL litesoundsglob.svg -e litesoundsglob.png
inkscape -z -w $XSMALL -h $XSMALL litesoundsloc.svg -e litesoundsloc.png
inkscape -z -w $XSMALL -h $XSMALL litesources.svg -e litesources.png
inkscape -z -w $XSMALL -h $XSMALL litesupport.svg -e litesupport.png
inkscape -z -w $XSMALL -h $XSMALL litesystemreport.svg -e litesystemreport.png
inkscape -z -w $XSMALL -h $XSMALL litetweaks.svg -e litetweaks.png
inkscape -z -w $XSMALL -h $XSMALL liteupdatesnotify.svg -e liteupdatesnotify.png
inkscape -z -w $XSMALL -h $XSMALL liteupdates.svg -e liteupdates.png
inkscape -z -w $XSMALL -h $XSMALL liteupgrade.svg -e liteupgrade.png
inkscape -z -w $XSMALL -h $XSMALL liteusermanager.svg -e liteusermanager.png
inkscape -z -w $XSMALL -h $XSMALL litewelcome.svg -e litewelcome.png
inkscape -z -w $XSMALL -h $XSMALL litewidget.svg -e litewidget.png

echo "All done."
echo
echo "Creating PNG directory ($XSMALL) ..."

mkdir -p $XSMALL
mv *.png $XSMALL/

echo
echo "Wait..."
sleep 2

echo
echo "Creating directory (ICONS) and organizing the icons..." 
echo

mkdir -p $INITIAL/$SUBDIR/
mv  $DEFAULT/ $INITIAL/$SUBDIR/
mv  $XLARGE/ $INITIAL/$SUBDIR/
mv  $NORMAL/ $INITIAL/$SUBDIR/
mv  $MEDIUM/ $INITIAL/$SUBDIR/
mv $XSMALL/ $INITIAL/$SUBDIR/

clear
echo "+"
echo "+"
echo "+ All done."
echo "+ All icons are in the ( $INITIAL/$SUBDIR/ ) directory."
echo "+"
echo "+"
echo "+ You can now choose the size of the icons to use on your system."
echo "+"
echo "+"
echo "+ ( ! ) IMPORTANT:"
echo "+ ( ! ) Make a backup of the original icons before replacing them."
echo "+"
echo "+ That's it for today. Goodbye :)"
echo "+"
echo "+"
exit

